#!/bin/bash

cat << "EOF"
/_  / ___  ___  __ _/_  / ___  ___  __ _  / /
  / /_/ _ \/ _ \/  ' \/ /_/ _ \/ _ \/  ' \/_/
 /___/\___/\___/_/_/_/___/\___/\___/_/_/_(_)
        ---Speedy Site Scanner---

Instructions:

1. Add each site into the websites file, DONT use 'www'  (ie: https://convertus.com)
2. Be patient
3. Look in the speedy_results/<website-name> directory and you will have results as .json

Author: nirzinger@convertus.com

EOF


IFS=$'\r\n' GLOBIGNORE='*' command eval  'websites=($(cat websites))'

request="https://www.googleapis.com/pagespeedonline/v5/runPagespeed?url="

for i in "${websites[@]}"
do
    echo "Running speed test on $i"
    dir=$( echo $i | cut -f1 -d"." ) 
    dir=$( echo $dir | cut -f2- -d"/" )
    dir=$( echo $dir | cut -f2- -d"/" )
    dir=speedy_results/$dir
    mkdir -p $dir 
    curl -o "$dir/$(date '+%d-%m-%Y-%H:%M:%S').json"  ${request}$i
done
