SpeedyScanner

---

#### How To

1. `git clone git@bitbucket.org:nirzinger/speedy-scanner.git`
2. `cd speedy-scanner`
3. Run `chmod +x speedy-scanner.sh` for good measure
4. Fire it up! `./speedy-scanner.sh`